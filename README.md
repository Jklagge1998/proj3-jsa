# proj3-JSA
Vocabulary anagrams game for primary school English language learners (ELL)

## Overview

A simple anagram game designed for English-language learning students in elementary and middle school. Students are presented with a list of vocabulary words (taken from a text file) and an anagram. The anagram is a jumble of some number of vocabulary words, randomly chosen. Students attempt to type words that can be created from the jumble. When a matching word is typed, it is added to a list of solved words. 

The vocabulary word list is fixed for one invocation of the server, so multiple students connected to the same server will see the same vocabulary list but may  have different anagrams.

## Authors 
* Revised version by Jackson Klagge (Jklagge@uoregon.edu).
* Initial version by M Young.
* Docker version added by R Durairajan. 

# Revision Tasks
* I replaced the form interaction (in flask_vocab.py) with AJAX interaction on each keystroke using flask_minijax.py (included in repository files but not used in the project. They are just there for reference). Source code for this project can be found at https://bitbucket.org/UOCIS322/proj3-jsa .

